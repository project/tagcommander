<?php

namespace Drupal\tagcommander\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form to configure module settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tagcommander_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tagcommander.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('tagcommander.settings');

    $form['tagcommander'] = [
      '#type' => 'details',
      '#title' => $this->t('TagCommander Settings'),
      '#open' => TRUE,
    ];

    $form['tagcommander']['head_container'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Head container URL'),
      '#description' => $this->t('Ex: //cdn.tagcommander.com/1234/tc_header_21.js (Placed in the head section)'),
      '#default_value' => $config->get('head_container'),
      '#maxlength' => 255,
    ];

    $form['tagcommander']['body_container'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body Footer container URL'),
      '#description' => $this->t('Ex: //cdn.tagcommander.com/1234/tc_footer_main_20.js (Placed at the end of the body)'),
      '#default_value' => $config->get('body_container'),
      '#maxlength' => 255,
    ];

    $form['tagcommander']['environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Select environment'),
      '#options' => [
        'develop' => $this->t('Development'),
        'review' => $this->t('Review'),
        'staging' => $this->t('Staging'),
        'prod' => $this->t('Production'),
      ],
      '#default_value' => $config->get('environment'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $config = $this->config('tagcommander.settings');

    if ($form_state->hasValue('head_container')) {
      $config->set('head_container', $form_state->getValue('head_container'));
    }

    if ($form_state->hasValue('body_container')) {
      $config->set('body_container', $form_state->getValue('body_container'));
    }

    if ($form_state->hasValue('environment')) {
      $config->set('environment', $form_state->getValue('environment'));
    }

    $config->save();
  }

}
