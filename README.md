# TAGCOMMANDER

Manage TagCommander datalayer and container(s) via Drupal configuration.

## INTRODUCTION

[TagCommander solution](https://www.commandersact.com/fr/solutions/tagcommander) is the tag management platform 
marketers are increasingly turning to as it gives them the ability to manage and deploy 
their tags, and harvest the data collected from them, without support from 
their IT teams.

TagCommander module provides the administration configuration to add your TagCommander ID and build the dataLayer 
with your tags.

* For a full description of the module, visit the project page:
  https://drupal.org/project/tagcommander
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/tagcommander

## REQUIREMENTS

* Config Ignore (https://drupal.org/project/config_ignore)

## RECOMMENDED MODULES

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

## CONFIGURATION

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## MAINTAINERS

This project has been sponsored by:

[Insign](https://www.drupal.org/insign)
